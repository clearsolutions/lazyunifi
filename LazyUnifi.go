package main

import (
	"container/list"
	"fmt"
	"io"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	portscanner "github.com/anvie/port-scanner"
	"golang.org/x/crypto/ssh"
)

func main() {
	fmt.Printf("----------------------------------------------------------------------\n")
	fmt.Printf("----------------Running LazyUnifi scanning for unifi------------------\n")
	fmt.Printf("----------------------------------------------------------------------\n")
	fmt.Printf("-----------------------By Clear Solutions BVBA------------------------\n")
	fmt.Printf("----------------------------------------------------------------------\n")
	var informurl string
	informurl = "" //SET THIS TO YOUR CONTROLLER INFORM COMMAND AKA "set-inform http://unifi.com:8080/inform"
	l := list.New()
	var iprange string
	//get local ip range
	host, _ := os.Hostname()
	addrs, _ := net.LookupIP(host)
	for _, addr := range addrs {
		if ipv4 := addr.To4(); ipv4 != nil {
			var localip string
			var x int
			localip = fmt.Sprint(ipv4)
			i := strings.LastIndex(localip, ".")
			x = i + 1
			iprange = localip[0:x]
		}
	}

	var wg sync.WaitGroup
	wg.Add(254)
	for i := 0; i < 254; i++ {
		go func(i int) {
			var num = i
			ps := portscanner.NewPortScanner(iprange+strconv.Itoa(num), 2*time.Second, 1)
			openedPorts := ps.GetOpenedPort(22, 22)
			for i := 0; i < len(openedPorts); i++ {
				ip := iprange + strconv.Itoa(num)
				//port := ":22"
				//fmt.Println(" ", ip, port, " [open]")
				l.PushFront(ip)
			}
			defer wg.Done()
		}(i)

	}
	wg.Wait()
	print("Done scanning\n")
	for e := l.Front(); e != nil; e = e.Next() {
		var user string
		var pass string
		user = "ubnt"
		pass = "ubnt"

		config := &ssh.ClientConfig{
			User: user,
			Auth: []ssh.AuthMethod{ssh.Password(pass)},
		}
		config.HostKeyCallback = ssh.InsecureIgnoreHostKey()
		var ip string
		ip = e.Value.(string) + ":22"
		conn, err := ssh.Dial("tcp", ip, config)
		if err == nil {
			runCommand(informurl, conn)
		} else {
			continue
		}
		defer conn.Close()

	}
	fmt.Println("Finished, refer to controller to proceed adoption")

}
func runCommand(cmd string, conn *ssh.Client) {
	sess, err := conn.NewSession()
	defer sess.Close()
	sessStdOut, err := sess.StdoutPipe()
	if err != nil {
		panic(err)
	}
	go io.Copy(os.Stdout, sessStdOut)
	sessStderr, err := sess.StderrPipe()
	if err != nil {
		panic(err)
	}
	go io.Copy(os.Stderr, sessStderr)
	err = sess.Run(cmd)
	if err != nil {
		panic(err)
	}
}
