# LazyUnifi

Tool for unifi installers that finds new unifi devices in the network and informs them

Usage:
1. Edit line 25: SET THIS TO YOUR CONTROLLER INFORM COMMAND AKA "set-inform http://unifi.com:8080/inform"
2. Compile with go 
3. Run ./LazyUnifi on the network 
4. Go to your controller and adopt all devices